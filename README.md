## Install

```sh
$ npm install
```

## Start development server

```sh
$ npm start
```

## Build for production

```sh
$ npm run build
```
