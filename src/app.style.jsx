export const styles = theme => ({
    root: {
        flexGrow: 1,
        overflow: 'hidden',
        marginTop: theme.spacing(8),
    },
    logo: {
        maxWidth: 160,
    },
    content: {
        padding: 0
    }
})