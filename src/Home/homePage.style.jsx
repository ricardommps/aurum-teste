export const styles = theme => ({
    root: {
        padding: '20px',
        background : "whitesmoke"
    },

    button: {
        margin: theme.spacing(1),
    },

    footer: {
        marginTop: theme.spacing(2)
    },

    input: {
        marginLeft: theme.spacing(1),
        paddingLeft: '8px',
        flex: 1,
        width: '95%',
        background: 'white'
    },

    iconButton: {
        padding: 10,
    }
})