import React, { Component, Fragment, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';

import {getAll} from '../redux/lawsuit/actions';
import {getAllCases, filterCases} from '../redux/lawsuit/selectors'

import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import FolderIcon from '@material-ui/icons/Folder';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import IconButton from '@material-ui/core/IconButton';

import {styles} from './homePage.style';

class HomePage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      visible: 10,
      searchInput:"",
      itens: []
    }
  }

  componentDidMount(){
    this.props.getAll();
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.lawsuit !== this.props.lawsuit){
      this.fetchData(this.props.lawsuit)
    }
  }
  fetchData = (data) => {
    this.setState({
      lawsuit:data
    })
  }

  handleClickView = (item) => {
   
    const location = {
      pathname: '/process/'+item.id,
      state: { item: item }
    }
    this.props.history.push(location)
  }

  handleFilter= () => {
    let searchInput = this.state.searchInput;
    this.fetchData(this.props.filterCases(searchInput))
  }

  handleLoadMore = () => {
    this.setState((prev) => {
      return {visible: prev.visible + 4};
    });
  }

  handleChange = event => {
    this.setState({ searchInput: event.target.value });
    if(event.target.value.length == 0){
      const timer = setTimeout(() => {
        this.handleFilter()
      }, 1000);
      return () => clearTimeout(timer);
    }
  };

  renderCases = (itens) => {
    return (
      <div>
          {this.renderTable(itens)}
      </div>
    )   
  }
  
  renderTable = (rows) => {
    const { classes } = this.props
    const {visible} = this.state
    let showButtonLoadMore = (this.state.visible < rows.length) ? true : false 
      return (
        <Fragment>
          <InputBase
            className={classes.input}
            placeholder="Pesquisar"
            onChange={this.handleChange}
            inputProps={{ 'aria-label': 'search' }}
          />
          <IconButton className={classes.iconButton} aria-label="search" onClick={() => this.handleFilter()}>
            <SearchIcon />
          </IconButton>
          <Table>
              <TableHead>
                  <TableRow>
                    <TableCell align="left">Tipo</TableCell>
                    <TableCell align="left">Titulo/Cliente</TableCell>
                    <TableCell align="left">Pasta</TableCell>
                    <TableCell align="left">Açaão Número</TableCell>
                    <TableCell align="left">Foro</TableCell>
                    <TableCell align="right"></TableCell>
                  </TableRow>
              </TableHead>
                <TableBody>
                  {rows.slice(0,visible).map(row => (
                    <TableRow key={row.id}>
                        <TableCell component="th" scope="row">
                          <FolderIcon />
                        </TableCell>
                        <TableCell align="left">{row.title}</TableCell>
                        <TableCell align="left">{row.file}</TableCell>
                        <TableCell align="left">{row.number}</TableCell>
                        <TableCell align="left">{row.lawsuitType}</TableCell>
                        <TableCell align="right">
                          <Button 
                            variant="contained" 
                            size="small" 
                            color="primary"
                            className={classes.button}
                            onClick={() => this.handleClickView(row)}
                          >
                            Ver
                          </Button>
                        </TableCell>
                    </TableRow>
                  ))}
                </TableBody>
          </Table>
          <div className={classes.footer}>
          {showButtonLoadMore && 
            <Button fullWidth onClick={this.handleLoadMore}>
              Carregar mais
            </Button>
          }
          </div>
        </Fragment>
      )
  }

  render() {
    const { classes } = this.props
    const {lawsuit} = this.state
    return (
      <Paper className={classes.root}>
          {lawsuit && this.renderCases(lawsuit)}
      </Paper>
    );
  }
}

const mapStateToProps = state => ({
  lawsuit: getAllCases(state),
  filterCases: filterCases(state)
});

const mapDispatchToProps = dispatch => bindActionCreators({
  getAll
},dispatch)

export default compose(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(HomePage);