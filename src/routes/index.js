import Home from '../Home/HomePage'
import ProcessDetails from '../ProcessDetails/ProcessDetailsPage'

const routes = [
    {
        path: '/home',
        component: Home
    },
    {
        path: '/process/:idProcess',
        component: ProcessDetails
    },
    {
        path: '/',
        component: Home,
        state: {}
    }

]

export default routes;