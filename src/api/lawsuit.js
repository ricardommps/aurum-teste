import axios from 'axios';
import * as constants from './constants';

axios.defaults.headers.common['x-frontend-test'] = 'aurumtest'
const BASE_URL = `${constants.REACT_APP_API_BACKEND}`;

class LawsuitService {
    all(){
        return axios
        .get(BASE_URL)
        .then(response => response.data)
        .catch(error => {
            throw error;
        });
    }
}

export default new LawsuitService();