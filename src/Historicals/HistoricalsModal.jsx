import React, { Component, Fragment } from 'react';
import Moment from 'react-moment';

import { withStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import IconButton from '@material-ui/core/IconButton';
import Cloud from '@material-ui/icons/Cloud';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import Tooltip from '@material-ui/core/Tooltip';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

import {styles} from './historicalsModal.style';

class HistoricalsModal extends Component {
    render() {
        const {openModal, handleClose, historicals, classes} = this.props
        return (
            <Dialog
                fullWidth={true}
                maxWidth='sm'
                open={openModal}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
                >
              <AppBar className={classes.appBar} style={{ background: 'transparent', boxShadow: 'none'}}>
                    <Toolbar>
                        <Typography variant="h6" className={classes.title}>
                            Históricos
                        </Typography>
                        <IconButton edge="end" color="primary" onClick={handleClose} aria-label="close">
                        <CloseIcon />
                        </IconButton>
                    </Toolbar>
                    </AppBar>
                <DialogContent>
                {historicals.map(item => (
                 <Paper className={classes.paper}>
                      <Grid container wrap="nowrap" spacing={1}>
                        <Grid item>
                            <Cloud/>
                        </Grid>
                        <Grid item xs zeroMinWidth>
                            <Typography noWrap>
                                <Box component="div" display="inline" p={1} m={1}>
                                    <Moment format="DD/MM/YYYY">
                                        {item.date}
                                    </Moment>
                                </Box>
                                <Tooltip title={item.description} placement="bottom-start" classes={{ tooltip: classes.customWidth }}>
                                    <Box component="div" display="inline">
                                        {item.description}
                                    </Box>
                                </Tooltip>
                            </Typography>
                        </Grid>
                        </Grid>  
                 </Paper>       
                ))}
                </DialogContent>
            </Dialog>
        )
    }
}

export default withStyles(styles) (HistoricalsModal)