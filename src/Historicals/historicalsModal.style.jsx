export const styles = theme => ({
    inline: {
        display: 'inline',
    },

    marginLeft: {
        marginLeft: theme.spacing(3),
    },

    root: {
        flexGrow: 1,
        overflow: 'hidden',
        padding: theme.spacing(0, 3),
    },

    paper: {
        margin: `${theme.spacing(1)}px auto`,
        padding: theme.spacing(2),
        background : "whitesmoke"
    },

    customWidth: {
        maxWidth: 500,
    },

    appBar: {
        position: 'relative',
    },

    title: {
        marginLeft: theme.spacing(2),
        flex: 1,
        color: "black"
    },
})