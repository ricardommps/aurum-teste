import React, { Component } from 'react';

import {Provider} from 'react-redux'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import store from './store';
import routes from './routes';

import {styles} from './app.style';
import { withStyles } from '@material-ui/core/styles';
import theme from './theme/theme';
import { ThemeProvider } from '@material-ui/styles';
import Grid from '@material-ui/core/Grid';
import logo from './logo.png';
const basename = process.env.PUBLIC_URL ? process.env.PUBLIC_URL : '/';

class App extends Component {
  render() {
    const {classes} = this.props
    return (
      <ThemeProvider theme={theme}>
        <Provider store={store}>
        <Grid container justify = "center" spacing={2} className={classes.root}>
          <Grid item xs={10}>
            <img src={logo} alt="logo" className={classes.logo} />
          </Grid>
            <BrowserRouter basename={basename}>
            <Grid item xs={10}>
                <Switch>
                  {
                    routes.map(route => (
                      <Route
                        key={route.path}
                        path={route.path}
                        component={route.component}
                      />
                    ))
                  }
                </Switch>
              </Grid>
            </BrowserRouter>
          </Grid>
        </Provider>
      </ThemeProvider>
    );
  }
}

export default withStyles(styles) (App);
