export const styles = theme => ({
    marginTop: {
        marginTop: theme.spacing(3),
    },

    root: {
        padding: '20px',
        background : "whitesmoke"
    },

    title: {
        marginTop: '6px'
    }
})