import React, { Component, Fragment } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Button from '@material-ui/core/Button';
import {InputBootstrap} from '../common/UI';
import HistoricalsModal from '../Historicals/HistoricalsModal';
import {styles} from './processDetails.style';
import AppBar from '@material-ui/core/AppBar';

class ProcessDetailsPage extends Component {

  constructor(props) {
    super(props)
    this.state = {
      openHistoricals: false,
      historicals: []
    }
  }

  handleHistoricalsView = (historicals) => {
    this.setState({
      openHistoricals: true,
      historicals:historicals
    })
  }

  handleCloseHistoricals = () => {
    this.setState({
      openHistoricals: false,
      historicals:[]
    })
  }

  handleGoBack = () => {
    this.props.history.goBack();
  }

  render() {
   const { classes } = this.props
   const item = this.props.location.state.item
   let viewHistoricals = item.historicals.length > 0 ? true : false
    return (
      <Fragment>
        <Box display="flex" justifyContent="flex-end" m={1} p={1}>
          <Box>
          <Button size="medium" className={classes.margin} onClick={() => this.handleGoBack()}>
            Voltar 
          </Button>
          </Box>
        </Box>
         <Box display="flex">
            <Box flexGrow={1}>
              <Typography variant="body1" gutterBottom className={classes.title}>
                {item.title}
              </Typography>
            </Box>
            {viewHistoricals &&
              <Box>
                <Button size="medium" className={classes.margin} onClick={() => this.handleHistoricalsView(item.historicals)}>
                  Visualizar históricos
                </Button>
              </Box>
            }
          </Box>
          <Paper className={classes.root}>
              <Grid container item xs={12} spacing={3}>
                <Grid item xs={4}>
                    <InputBootstrap  
                      label={'Número do Processo'} 
                      value={this.props.location.state.item.number} id="bootstrap-input-number" disabled/>
                </Grid>
                <Grid item xs={4}>
                    <InputBootstrap 
                      label={'Vara'} 
                      value={this.props.location.state.item.court} id="bootstrap-input-court" disabled/>
                </Grid>
                <Grid item xs={4}>
                    <InputBootstrap 
                      label={'Ação'} 
                      value={this.props.location.state.item.lawsuitType} id="bootstrap-input-lawsuitType" disabled/>
                </Grid>
            </Grid>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={4}>
                  <InputBootstrap 
                    label={'Titulo'} 
                    value={this.props.location.state.item.title} id="bootstrap-input-title" disabled/>
              </Grid>
              <Grid item xs={4}>
                  <InputBootstrap 
                    label={'Tipo'} 
                    value={this.props.location.state.item.type} id="bootstrap-input-type" disabled/>
              </Grid>
              <Grid item xs={4}>
                  <InputBootstrap 
                    label={'Valor'} 
                    value={this.props.location.state.item.amount} id="bootstrap-input-amount" disabled/>
              </Grid>
            </Grid>
            <Grid container item xs={12} spacing={3}>
              <Grid item xs={4}>
                  <InputBootstrap 
                    label={'Lorem ipsum'} 
                    value={'Lorem ipsum'} id="bootstrap-input-lorem1" disabled/>
              </Grid>
              <Grid item xs={4}>
                  <InputBootstrap 
                    label={'Lorem ipsum'} 
                    value={'Lorem ipsum'} id="bootstrap-input-lorem2" disabled/>
              </Grid>
              <Grid item xs={4}>
                  <InputBootstrap 
                    label={'Lorem ipsum'} 
                    value={'Lorem ipsum'} id="bootstrap-input-lorem3" disabled/>
              </Grid>
            </Grid>
          </Paper>
          {viewHistoricals &&
          <HistoricalsModal 
            openModal={this.state.openHistoricals} 
            handleClose={this.handleCloseHistoricals}
            historicals={this.state.historicals}
          />
          }
      </Fragment>
    );
  }
}

export default withStyles(styles) (ProcessDetailsPage)