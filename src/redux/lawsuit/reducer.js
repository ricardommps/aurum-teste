import types from '../../constants/actionTypes.constants'

const INITIAL_STATE = {
    itens : {}
}

export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case types.LOAD_ALL_LAWSUIT:
            return {
                ...state,
                itens: action.payload
            }
        default:
            return state
    }
}