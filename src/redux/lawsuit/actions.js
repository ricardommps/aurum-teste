import lawsuitService from '../../api/lawsuit'
import types from '../../constants/actionTypes.constants'

export const getAll = (
    service = lawsuitService
) => dispatch =>
    service.all()
        .then(data => dispatch({
            type: types.LOAD_ALL_LAWSUIT,
            payload: data
        }))