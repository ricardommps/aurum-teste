export const getAllCases = state => state.lawsuitReducer.itens.cases || []

export const filterCases = state => searchInput => {
    if(searchInput) {
        const itens = state.lawsuitReducer.itens.cases
        let filteredData =  itens.filter(value => {
            return (
              value.type.toLowerCase().includes(searchInput.toLowerCase()) ||
              value.title.toLowerCase().includes(searchInput.toLowerCase()) ||
              value.court.toLowerCase().includes(searchInput.toLowerCase()) ||
              value.number
              .toString()
              .toLowerCase()
              .includes(searchInput.toLowerCase())
            )}) 
        return filteredData
    }else{
        return state.lawsuitReducer.itens.cases
    }
    
}

