import { combineReducers } from 'redux';
import lawsuitReducer from './lawsuit/reducer'
import ui from './ui/reducer'

export default combineReducers({
    lawsuitReducer,
    ui
})