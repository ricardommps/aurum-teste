import { createMuiTheme } from '@material-ui/core/styles';


export default createMuiTheme({
  palette: {
    primary: { main: '#1b1b1b' }
  },
  overrides: {
    MuiTooltip: {
      tooltip: {
        fontSize: "14px",
        color: "black",
        backgroundColor: "#fff"
      }
    }
  }
});